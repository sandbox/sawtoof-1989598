/**
 * @file
 * README file for Workbench Access Automenu.
 */

Workbench Access Automenu
Provides automatic section assignment when using the Workbench Access menu scheme.