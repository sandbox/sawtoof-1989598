<?php

/**
 * @file
 * Workbench Access Automenu admin file.
 */

/**
 * Settings form for Workbench Access Automenu configuration.
 */
function workbench_access_automenu_settings_form($form_state) {

	$form['automenu_rebuild']  = array(
    '#type' => 'fieldset',
    '#title' => t('Rebuild automenu sections'),
	);
	
	$form['automenu_rebuild']['submit']  = array(
    '#type' => 'submit',
    '#value' => t('Rebuild'),
		'#submit' => array('workbench_access_automenu_rebuild'),  
	);
	
	$form['automenu_settings']  = array(
    '#type' => 'fieldset',
    '#title' => t('Automenu settings'),
	);
	
	$form['automenu_settings']['enable_default']  = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('workbench_access_automenu_enable_default', 0),
    '#title' => t('Enable default mlid for nodes without a menu link'),
	);
	
	$form['automenu_settings']['default_mlid']  = array(
    '#type' => 'textfield',
    '#title' => t('Default mlid'),
		'#default_value' => variable_get('workbench_access_automenu_default_mlid','main-menu'),  
	);
	
	$form['submit']  = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
		'#submit' => array('workbench_access_automenu_submit'),  
	);
	
	return $form;
}

/**
 * Rebuild access node table
 *
 * Do nothing if access scheme is not set to menu
 *
 */
function workbench_access_automenu_rebuild() {
	
	if (variable_get('workbench_access', 0) != 'menu') {
		drupal_set_message("Workbench access scheme is not set to menu.", 'warning');
		return;
	}

	db_truncate('workbench_access_node')->execute();
	
	$types = node_type_get_names();
	$workbench_access_node_types = array();
	foreach ($types as $key => $value) {
		if (variable_get('workbench_access_node_type_' . $key, 0) == 1) {
			$workbench_access_node_types[] = $key;
		}
	}
	
	$query = new EntityFieldQuery();
	$query->entityCondition('entity_type', 'node')
		->entityCondition('bundle', $workbench_access_node_types, 'IN')
		->propertyCondition('status', 1);
	$result = $query->execute();

	if (!empty($result['node'])) {
		$nids = array_keys($result['node']);

		foreach ($nids as $nid) {
			db_delete('workbench_access_node')
				->condition('nid', $nid)
				->execute();

			$mlid = db_select('menu_links' , 'ml')
				->condition('ml.link_path' , 'node/' . $nid)
				->fields('ml' , array('mlid'))
				->execute()
				->fetchField();

			if (($mlid == '') && (variable_get('workbench_access_automenu_enable_default', 0) == 1)) {
					 $mlid = variable_get('workbench_access_automenu_default_mlid', 'main-menu');
			}

			$record = array(
				'nid' => $nid,
				'access_id' => $mlid,
				'access_scheme' => 'menu',
			);
			drupal_write_record('workbench_access_node', $record);
		}
	}
	drupal_set_message("Automenu sections have been successfully rebuilt.");
}

/**
 * Set configuration.
 */
function workbench_access_automenu_submit($form, $form_state) {
	$enable_default = $form_state['values']['enable_default'];
	$default_mlid = $form_state['values']['default_mlid'];
	variable_set('workbench_access_automenu_enable_default', $enable_default);
	variable_set('workbench_access_automenu_default_mlid', $default_mlid);
	drupal_set_message("The configuration options have been saved.");
}
